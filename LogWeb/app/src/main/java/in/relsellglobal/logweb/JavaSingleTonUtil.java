package in.relsellglobal.logweb;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import in.relsellglobal.logweb.constants.AppContants;

public class JavaSingleTonUtil {

    private static JavaSingleTonUtil javaSingleTonUtil;

    private static final String ANDROID_CHANNEL_ID = "android_channel_id";
    private static final String ANDROID_CHANNEL_NAME = "ANDROID CHANNEL";

    public static JavaSingleTonUtil getJavaSingleTonUtil() {
        if(javaSingleTonUtil == null) {
            javaSingleTonUtil = new JavaSingleTonUtil();
        }
        return javaSingleTonUtil;
    }

    public void sendFCMRegistrationToServer(String token,String cookie) {
        // TODO: Implement this method to send token to your app server.
        String baseUrl = AppContants.BROWSER_KEYS.ServerUrl;
        String path = AppContants.BROWSER_KEYS.RegistrationIdStorageLocation;
        String urlToLoad = baseUrl + path;
//        cookie = prefs.getCookie();
        String[] data = new String[3];
        data[0] = urlToLoad;
//        data[1] = cookie;
        data[1] = token;

        data[2] = cookie;

        SendRegId sendRegId = new SendRegId();
        sendRegId.execute(data);
    }


    private class SendRegId extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... data) {
            Boolean isTokenSent = false;

            try {

                // This is getting the url from the string we passed in
                URL url = new URL(data[0]);
//                String token = data[1];
                HashMap<String, String> dataToBeSent = new HashMap<>();
                dataToBeSent.put("rk",data[1]);

                // Create the urlConnection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                urlConnection.setRequestMethod("POST");
                urlConnection.setRequestProperty("Cookie",data[2]);
                urlConnection.setRequestProperty(AppContants.BROWSER_KEYS.contentType,AppContants.BROWSER_KEYS.contentTypeValue);
//                urlConnection.setRequestProperty("content-length","38");
                //urlConnection.setInstanceFollowRedirects(false);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                Log.v("TAG",dataToBeSent.toString());

                String dataStr = dataToBeSent.toString();
                dataStr = dataStr.substring(1,dataStr.length() - 1);


                // Send the post body
                if (dataToBeSent != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(dataStr);
                    writer.flush();
                    writer.close();
                }

                int statusCode = urlConnection.getResponseCode();
                Log.d("TAG","Status Code "+statusCode);
                if(statusCode == HttpURLConnection.HTTP_OK){
                    isTokenSent = true;
                } else {
                    isTokenSent = false;
                }

            } catch (Exception e) {
                Log.d("TAG", e.getLocalizedMessage());
                System.out.println("TOKEN IS NOT SENT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
            return isTokenSent;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
//            Toast.makeText(MyFirebaseInstanceIDService.this, "Token Sent!", Toast.LENGTH_SHORT).show();
            if(aBoolean){
                System.out.println("TOKEN IS SENT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            } else{
                System.out.println("TOKEN IS NOT SENT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
        }
    }


    public static void showNotification(Context context,String title,String msg) {




        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context,ANDROID_CHANNEL_ID);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            int priority = NotificationManager.IMPORTANCE_HIGH;
            if(Build.VERSION.SDK_INT>=26){
                NotificationChannel notificationChannel = new NotificationChannel(ANDROID_CHANNEL_ID,"LOGWEB",priority);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.GREEN);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                mBuilder.setChannelId(ANDROID_CHANNEL_ID);
                mNotificationManager.createNotificationChannel(notificationChannel);
            }
            assert mNotificationManager != null;


            mBuilder.setSmallIcon(R.mipmap.ic_launcher_round)
                    .setColor(Color.parseColor("#048d9f"))
                    .setContentTitle(title)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationCompat.PRIORITY_HIGH)
                    .setContentText(msg);
        } else {
            mBuilder.setSmallIcon(R.mipmap.ic_launcher_round)
                    .setContentTitle("title")
                    .setAutoCancel(true)
                    .setContentText(msg);
        }
        Intent resultIntent = new Intent(context, WelcomeActivity.class);
        resultIntent.putExtra("NOTIFICATION","TRUE");

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        mNotificationManager.notify(1, mBuilder.build());




    }

}






