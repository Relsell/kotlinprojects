package `in`.relsellglobal.logweb

import `in`.relsellglobal.logweb.constants.AppContants
import `in`.relsellglobal.logweb.dummy.DummyContent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.firebase.iid.FirebaseInstanceId

class MainActivity : AppCompatActivity(),ItemFragment.OnListFragmentInteractionListener {
    var prefs: PrefClass? = null
    override fun onListFragmentInteraction(item: DummyContent.DummyItem?) {
        Toast.makeText(this,"This is "+item?.content,Toast.LENGTH_LONG).show();
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prefs = PrefClass(this)
        val cookie = prefs!!.cookie
        println(cookie)

        val refreshedToken = FirebaseInstanceId.getInstance().token
        JavaSingleTonUtil.getJavaSingleTonUtil().sendFCMRegistrationToServer(refreshedToken,cookie);



        val responseList:ArrayList<String> = intent.getStringArrayListExtra(AppContants.BundleKeys.responseList)
        var i = WebFragmentJava()
        val b = Bundle()
        b.putStringArrayList(AppContants.BundleKeys.responseList,responseList)
        i.setArguments(b)
        supportFragmentManager.beginTransaction().replace(R.id.root,i).commit()
    }




}
