package `in`.relsellglobal.logweb

import android.content.Context
import android.content.SharedPreferences

class PrefClass(context: Context){
    val PREFS_FILENAME = "LogWebPrefs"
    val COOKIE = "cookie"
    val PATH = "path"
    val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0);

    var cookie: String
        get() = prefs.getString(COOKIE,"NO-COOKIE")
        set(value) = prefs.edit().putString(COOKIE,value).apply();

    var path: String
        get()  = prefs.getString(PATH,"NO-PATH")
        set(value2) = prefs.edit().putString(PATH,value2).apply()
}