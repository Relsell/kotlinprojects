/*
 * Copyright (c) 2018. Relsell Global
 */

package `in`.relsellglobal.logweb


import `in`.relsellglobal.logweb.constants.AppContants
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class WebFragment : Fragment() {

    var mywebView: WebView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_web, container, false)

        mywebView = view.findViewById(R.id.webview)

        return view;
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        var args = getArguments()
        var responseList:ArrayList<String> = args!!.getStringArrayList(AppContants.BundleKeys.responseList)
        var url = responseList!!.get(0)
        var cookieValue = responseList!!.get(1)
        var additionalHeaders: HashMap<String,String> = HashMap()
        //additionalHeaders.put(AppContants.BROWSER_KEYS.contentType,AppContants.BROWSER_KEYS.contentTypeValue)
        additionalHeaders.put(AppContants.BROWSER_KEYS.SetCookie,cookieValue)

        mywebView!!.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                view?.loadUrl(request.toString(),additionalHeaders)
                return false
            }
        };


        val webSettings = mywebView!!.getSettings()
        webSettings.javaScriptEnabled = true

        mywebView!!.loadUrl(AppContants.BROWSER_KEYS.ServerUrl+url,additionalHeaders)
    }

}
