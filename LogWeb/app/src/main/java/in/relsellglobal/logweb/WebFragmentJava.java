package in.relsellglobal.logweb;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.ArrayList;
import java.util.HashMap;

import in.relsellglobal.logweb.constants.AppContants;
import in.relsellglobal.logweb.firebase.MyFirebaseInstanceIDService;
import pl.droidsonroids.gif.GifImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class WebFragmentJava extends Fragment {

    private WebView mWebView;
    private GifImageView gifIMView;


    public WebFragmentJava() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_web_fragment_java, container, false);

        mWebView = view.findViewById(R.id.webview);
        gifIMView = view.findViewById(R.id.gifViewId);

        return view;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle b = getArguments();
        ArrayList<String> responseList = b.getStringArrayList(AppContants.BundleKeys.responseList);
        System.out.println("THIS");
        String url = responseList.get(0);
        String cookieVal = responseList.get(1);
        final HashMap<String,String> hm = new HashMap();
        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setCookie("Cookie", cookieVal);
        cookieManager.setAcceptThirdPartyCookies(mWebView,true);
        cookieManager.flush();
        hm.put("Cookie",cookieVal);
        mWebView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url,hm);
                return false;
            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view,request);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if(url != null && url.contains(AppContants.BROWSER_KEYS.indexUrl)) {
                    mWebView.setVisibility(View.VISIBLE);
                    gifIMView.setVisibility(View.GONE);
                }
            }
        });

        mWebView.loadUrl(AppContants.BROWSER_KEYS.ServerUrl+url,hm);
        mWebView.getSettings().setJavaScriptEnabled(true);
        MyFirebaseInstanceIDService myFirebaseInstanceIDService = new MyFirebaseInstanceIDService();
        myFirebaseInstanceIDService.onTokenRefresh();

    }



}
