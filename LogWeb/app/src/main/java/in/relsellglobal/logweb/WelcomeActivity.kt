/*
 * Copyright (c) 2018. Relsell Global
 */

package `in`.relsellglobal.logweb

import `in`.relsellglobal.logweb.constants.AppContants
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_welcome.*
import java.io.*
import java.net.HttpURLConnection
import java.net.URL


class WelcomeActivity : AppCompatActivity() {
    var prefs: PrefClass? = null
    var isLoggedIn = false
    var activateSigninButton = false
    val CONNECTON_TIMEOUT_MILLISECONDS = 60000

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)



        prefs = PrefClass(this)

        var edtUsername = findViewById<EditText>(R.id.edt_username)
        var edtPassword = findViewById<EditText>(R.id.edt_password)
        var j = VideoFragment()
        supportFragmentManager.beginTransaction().replace(R.id.bottomLayout,j).commit();
        var btnLogin = findViewById<ImageView>(R.id.btn_login)
        var enterCredentials = findViewById<LinearLayout>(R.id.user_pass)
        var loginLayout = findViewById<LinearLayout>(R.id.login_layout)

        // if control reaches here directly from notification
        // execute splash task directly from here

//        JavaSingleTonUtil.showNotification(this, "TITLE", "Hello world!")

        if("TRUE".equals(getIntent().getStringExtra("NOTIFICATION"))){
            if(prefs!!.cookie!="NO-COOKIE"){
                var savedCredentials: ArrayList<String> = ArrayList()
                savedCredentials.add(prefs!!.path)
                savedCredentials.add(prefs!!.cookie)
                var intent = Intent(this@WelcomeActivity, MainActivity::class.java)
                intent.putStringArrayListExtra(AppContants.BundleKeys.responseList,savedCredentials)
                startActivity(intent)
                finish()
            } else{
                enterCredentials.visibility = View.VISIBLE
                activateSigninButton = true;
            }
        }



        btnLogin.setOnClickListener(){
            if(activateSigninButton){
                loginLayout.visibility = View.VISIBLE
                btnLogin.setImageDrawable(ContextCompat.getDrawable(this@WelcomeActivity,R.mipmap.button_login2))
                loginLayout.setBackground(ContextCompat.getDrawable(this@WelcomeActivity,R.drawable.user_login_layout_background))
                val url = arrayOf(AppContants.BROWSER_KEYS.loginUrl,edtUsername.text.toString(),edtPassword.text.toString())
                var t = SplashTask()
                t.execute(url)
            } else{
                if(prefs!!.cookie!="NO-COOKIE"){
                    var savedCredentials: ArrayList<String> = ArrayList()
                    savedCredentials.add(prefs!!.path)
                    savedCredentials.add(prefs!!.cookie)
                    var intent = Intent(this@WelcomeActivity, MainActivity::class.java)
                    intent.putStringArrayListExtra(AppContants.BundleKeys.responseList,savedCredentials)
                    startActivity(intent)
                    finish()
                } else{
                    enterCredentials.visibility = View.VISIBLE
                    btnLogin.setImageDrawable(ContextCompat.getDrawable(this@WelcomeActivity,R.mipmap.button_login2))
                    loginLayout.setBackground(ContextCompat.getDrawable(this@WelcomeActivity,R.drawable.user_login_layout_background))
                    activateSigninButton = true;
                }
            }


            }



    }

    public fun loggedIn() {

    }

    public fun notLoggedIn(){

    }


    inner class SplashTask() : AsyncTask<Array<String>, Void, ArrayList<String>>() {

        fun Any.toast(context: Context, duration: Int = Toast.LENGTH_SHORT): Toast {
            return Toast.makeText(context, this.toString(), duration).apply { show() }
        }




        override fun doInBackground(vararg urls: Array<String>?): ArrayList<String> {

            var responseList: ArrayList<String> = ArrayList();
            var urlConnection: HttpURLConnection? = null



            try {
                val url = URL(urls[0]!![0])
                var username = urls[0]!![1]
                var password = urls[0]!![2]
                var path = ""
                var cookie = ""
                var code = 100


                urlConnection = url.openConnection() as HttpURLConnection
                urlConnection.setRequestMethod("GET")
                urlConnection.setRequestProperty(AppContants.BROWSER_KEYS.contentType,AppContants.BROWSER_KEYS.contentTypeValue)
                urlConnection.setRequestProperty("content-length","38")
                urlConnection.connectTimeout = CONNECTON_TIMEOUT_MILLISECONDS
                urlConnection.readTimeout = CONNECTON_TIMEOUT_MILLISECONDS
                urlConnection.instanceFollowRedirects = false
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);

                val os = urlConnection.getOutputStream()
                val writer = BufferedWriter(
                        OutputStreamWriter(os, "UTF-8"))
                writer.write(AppContants.BROWSER_KEYS.testUsernameKey+"="+username.trim()+"&"+AppContants.BROWSER_KEYS.testPasswordKey+"="+password.trim())


                writer.flush()
                writer.close()
                os.close()

               code = urlConnection.getResponseCode()
                if(code.equals(302)){
                    isLoggedIn = true
                } else{
                    isLoggedIn = false
                }




                val headerFields: Map<String, List<String>> = urlConnection.getHeaderFields()
                val headerFieldsSet: Set<String> = headerFields.keys
                val hearerFieldsIter = headerFieldsSet.iterator()


                while (hearerFieldsIter.hasNext()) {

                    val headerFieldKey = hearerFieldsIter.next()

                    if (AppContants.BROWSER_KEYS.SetCookie.equals(headerFieldKey, ignoreCase = true)) {

                        val headerFieldValue = headerFields[headerFieldKey]

                        for (headerValue in headerFieldValue!!) {

                            println("Cookie Found...")

                            val fields = headerValue.trim().split(";".toRegex()).dropLastWhile({ it.isEmpty() }).toTypedArray()

                              cookie = fields[0]+";"
                              responseList.add(cookie)

                              prefs!!.cookie = cookie


                        }

                    }
                    if(AppContants.BROWSER_KEYS.location.equals(headerFieldKey, ignoreCase = true)){
                        path = headerFields[headerFieldKey]!!.get(0)
                        responseList.add(path)

                        prefs!!.path = path
                    }
                }



            } catch (ex: Exception) {

               ex.printStackTrace()

            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect()
                }
            }
            return responseList
        }

        override fun onPostExecute(result: ArrayList<String>?) {
            if(isLoggedIn){
                var intent = Intent(this@WelcomeActivity, MainActivity::class.java)
                intent.putStringArrayListExtra(AppContants.BundleKeys.responseList,result)
                startActivity(intent)
                finish()
            } else{
                "Wrong Credentials!".toast(this@WelcomeActivity)
                edt_password.setText("")
                btn_login.setBackgroundResource(R.drawable.user_pass_background)
            }

        }
    }

    fun streamToString(inputStream: InputStream): String {

        val bufferReader = BufferedReader(InputStreamReader(inputStream))
        var line: String
        var result = ""

        try {
            do {
                line = bufferReader.readLine()
                if (line != null) {
                    result += line
                }
            } while (line != null)
            inputStream.close()
        } catch (ex: Exception) {

        }

        return result
    }

}
