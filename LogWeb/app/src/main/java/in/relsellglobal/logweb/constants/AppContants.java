package in.relsellglobal.logweb.constants;

public class AppContants {
    public interface BROWSER_KEYS {
        String location = "location";
        String SetCookie = "Set-Cookie";
        String loginUrl = "http://ec2-18-218-177-111.us-east-2.compute.amazonaws.com/j_spring_security_check";
        String contentType = "content-type";
        String contentTypeValue = "application/x-www-form-urlencoded";
        String testUsernameKey = "j_username";
        String testPasswordKey = "j_password";
        String testUsername = "tester";
        String testPassword = "tester1@1";
        String ServerUrl = "http://ec2-18-218-177-111.us-east-2.compute.amazonaws.com";
        String RegistrationIdStorageLocation = "/user/rk";
        String indexUrl = "/index";
    }

    public interface BundleKeys  {
        String responseList = "responseList";
    }

}
