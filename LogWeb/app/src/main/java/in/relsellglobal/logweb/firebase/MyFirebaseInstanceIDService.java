package in.relsellglobal.logweb.firebase;

import android.os.AsyncTask;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import in.relsellglobal.logweb.constants.AppContants;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
//    private PrefClass prefs;
    private String cookie;
    int CONNECTON_TIMEOUT_MILLISECONDS = 6000;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
//        prefs = new PrefClass(this);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        //sendRegistrationToServer(refreshedToken);
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     * <p>
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
    private void sendRegistrationToServer(String token) {
        // TODO: Implement this method to send token to your app server.
        String baseUrl = AppContants.BROWSER_KEYS.ServerUrl;
        String path = AppContants.BROWSER_KEYS.RegistrationIdStorageLocation;
        String urlToLoad = baseUrl + path;
//        cookie = prefs.getCookie();
        String[] data = new String[2];
        data[0] = urlToLoad;
//        data[1] = cookie;
        data[1] = token;

        SendRegId i = new SendRegId();
        i.execute(data);
    }

    class SendRegId extends AsyncTask<String, Void, Boolean> {

        @Override
        protected Boolean doInBackground(String... data) {
            Boolean isTokenSent = false;

            try {

                // This is getting the url from the string we passed in
                URL url = new URL(data[0]);
//                String token = data[1];
                HashMap<String, String> dataToBeSent = new HashMap<>();
                dataToBeSent.put("token",data[1]);

                // Create the urlConnection
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();


                urlConnection.setRequestMethod("GET");
                urlConnection.setRequestProperty(AppContants.BROWSER_KEYS.contentType,AppContants.BROWSER_KEYS.contentTypeValue);
//                urlConnection.setRequestProperty("content-length","38");
                urlConnection.setInstanceFollowRedirects(false);
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);


                // Send the post body
                if (data != null) {
                    OutputStreamWriter writer = new OutputStreamWriter(urlConnection.getOutputStream());
                    writer.write(dataToBeSent.toString());
                    writer.flush();
                    writer.close();
                }

                int statusCode = urlConnection.getResponseCode();
                if(statusCode == 302){
                    isTokenSent = true;
                } else {
                    isTokenSent = false;
                }
                System.out.print(statusCode);


            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage());
                System.out.println("TOKEN IS NOT SENT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
            return isTokenSent;
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
//            Toast.makeText(MyFirebaseInstanceIDService.this, "Token Sent!", Toast.LENGTH_SHORT).show();
            if(aBoolean){
                System.out.println("TOKEN IS SENT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            } else{
                System.out.println("TOKEN IS NOT SENT>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            }
        }
    }


}