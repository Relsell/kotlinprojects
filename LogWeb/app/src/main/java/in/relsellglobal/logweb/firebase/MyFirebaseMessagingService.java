package in.relsellglobal.logweb.firebase;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import in.relsellglobal.logweb.JavaSingleTonUtil;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.d(TAG, "From: " + remoteMessage.getFrom());


        if(remoteMessage.getData() != null) {

            if (remoteMessage.getData().size() > 0) {
                JavaSingleTonUtil.showNotification(getApplicationContext(), "TITLE", remoteMessage.getData().toString());
            }
        } else {
            JavaSingleTonUtil.showNotification(getApplicationContext(),"TITLE",remoteMessage.toString());
        }


    }
}
